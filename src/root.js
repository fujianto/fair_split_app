import React from 'react'

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import MainLayout from './components/layout/main-layout'
import Home from './pages/home'
import Login from './pages/login'
import NewOrder from './pages/new-order'
import AddParticipant from './pages/add-participant'
import FinishCalculate from './pages/finish-calculate'
import History from './pages/history'
import Settings from './pages/settings'


function Root() {
  return (
    <>
      <Router >
        <Switch>
          <Route path="/login">
            <MainLayout>
              <Login />
            </MainLayout>
          </Route>
          <Route path="/about">
            <MainLayout>
              <Home />
            </MainLayout>
          </Route>
          <Route path="/settings">
            <MainLayout>
              <Settings />
            </MainLayout>
          </Route>
          <Route path="/history">
            <MainLayout>
              <History />
            </MainLayout>
          </Route>
          <Route path="/finish-calculate">
            <MainLayout>
              <FinishCalculate />
            </MainLayout>
          </Route>
          <Route path="/add-participant">
            <MainLayout>
              <AddParticipant />
            </MainLayout>
          </Route>
          <Route path="/new-order">
            <MainLayout>
              <NewOrder />
            </MainLayout>
          </Route>
          <Route path="/">
            <MainLayout>
              <Home />
            </MainLayout>
          </Route>
        </Switch>
      </Router >
    </>
  );
}

export default Root;
