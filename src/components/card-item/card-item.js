
import React from 'react'

export default function CardItem({ name, orderName, price, actionButton }) {
  return (
    <div className="p-2 bg-gray-300 mb-2 flex flex-nowrap items-center">
      <div className="w-full flex flex-col">
        <h3 className="mb-1">{name}</h3>
        <h4>{orderName} {price}</h4>
      </div>

      <div>
        {
          actionButton ? actionButton : <i onClick={() => { }} className="fa fa-trash hover:text-red-500"></i>
        }
      </div>
    </div>
  )
}
