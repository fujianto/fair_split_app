import React from 'react'
import PropTypes from 'prop-types'

import './style.css'

function MenuButton(props) {
  return (
    <div className={`px-1 text-center mb-3 ${props.wrapperClass}`} onClick={() => props.onClick()}>
      <div className={`bg-green-500 hover:bg-green-400 text-white flex flex-col px-3 py-12 rounded-xl cursor-pointer ${props.buttonClass}`}>
        <div className="mb-3">{props.icon}</div>
        <h5>{props.title}</h5>
      </div>
    </div>
  )
}

MenuButton.defaultProps = {
  icon: <i className="fa fa-save fa-3x"></i>,
  title: 'Button',
  onClick: () => { console.log(`Clicking Menu Button`) },
  buttonClass: ''
}

MenuButton.prototype = {
  /** Button Icon */
  icon: PropTypes.node,
  /** Button Title */
  title: PropTypes.string,
  /** onClick Function */
  onClick: PropTypes.func,
  /** Button Class */
  buttonClass: PropTypes.string
}

export default MenuButton