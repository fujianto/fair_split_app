import React from 'react'

export default function Button(props) {
  return (
    <>
      <button className={`bg-green-500 hover:bg-green-400 text-white py-1.5 px-4 rounded cursor-pointer ${props.className}`} onClick={() => props.onClick()}>{props.title}</button>
    </>
  )
}

Button.defaultProps = {
  title: 'Button',
  className: '',
  onClick: () => { }
}
