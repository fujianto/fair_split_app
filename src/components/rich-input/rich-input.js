import React from 'react'

export default function RichInput({ type, label, value, placeholder }) {
  return (
    <div>
      <div className="flex flex-wrap justify-between">
        <label className="text-sm">{label}</label>
        <i className="fa fa-question-circle-o"></i>
      </div>

      <input className="border border-gray-400 w-full p-1 rounded" type={type ? type : "text"} placeholder={placeholder} value={value} />
    </div>
  )
}

RichInput.defaultProps = {
  type: 'text',
  label: 'Title',
  placeholder: null,
  value: null,
}