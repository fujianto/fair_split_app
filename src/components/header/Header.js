import React from 'react'
import {
  Link,
} from "react-router-dom";

export default function Header() {
  return (
    <div className="text-center py-3 lg:py-5 mb-5 text-2xl bg-green-500 text-white">
      <Link to="/">
        <h2>FairSplit <i className="fa fa-cutlery fa-lg text-yellow-300"></i></h2>
      </Link>
    </div>
  )
}
