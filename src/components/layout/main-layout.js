import React from 'react'
import Header from '../header'

export default function MainLayout(props) {
  return (
    <div className="container">
      <Header />
      <div className="mx-auto w-10/12 lg:w-1/2">
        {props.children}
      </div>
    </div>
  )
}
