import React from 'react'

export default function ListCard({ data, render }) {

  return (
    <div className="flex flex-nowrap flex-col w-full">
      {render(data)}
    </div>
  )
}


ListCard.defaultProps = {
  data: [],
  render: null
}