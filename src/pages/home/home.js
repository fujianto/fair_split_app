import React from 'react'
import { useHistory } from "react-router-dom";
import './style.css'
import MenuButton from '../../components/buttons/menu-button'
import logo from './logo.png';

function Home() {
  const history = useHistory();

  const handleButtonClick = (route) => {
    history.push(route)
  }

  return (
    <>
      <div className="mb-5 hero-content">
        <h3 className="text-center">Calculate your group lunch fairly and easily. Say goodbye to manually calculating your own share!</h3>

      </div>
      <div className="menu-wrapper flex flex-wrap">
        <MenuButton onClick={() => handleButtonClick('/new-order')} wrapperClass="w-1/2 lg:w-1/4" title="Calculate" icon={<i className="fa fa-calculator fa-3x"></i>} />
        <MenuButton onClick={() => handleButtonClick('/history')} wrapperClass="w-1/2 lg:w-1/4" title="History" icon={<i className="fa fa-line-chart fa-3x"></i>} />
        <MenuButton onClick={() => handleButtonClick('/settings')} wrapperClass="w-1/2 lg:w-1/4" title="Preferences" icon={<i className="fa fa-cog fa-3x"></i>} />
        <MenuButton onClick={() => handleButtonClick('/about')} wrapperClass="w-1/2 lg:w-1/4" title="About" icon={<i className="fa fa-question-circle-o fa-3x"></i>} />
      </div>
    </>
  )
}

export default Home