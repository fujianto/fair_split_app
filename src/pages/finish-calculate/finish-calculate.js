import React from 'react'
import { useHistory } from "react-router-dom";

import ListCard from '../../components/list-card'
import CardItem from '../../components/card-item'
import Button from '../../components/buttons/button'

export default function FinishCalculate(props) {
  const data = [
    {
      name: 'Rocky Balboa',
      orderName: 'McNuggets',
      price: '12000'
    },
    {
      name: 'John Rambo',
      orderName: 'KFC Super',
      price: '45000'
    }
  ]
  const history = useHistory()

  const renderListOrderItem = (data) =>
    <>
      {
        data ? data.map(({ name, price, orderName }) =>
          <CardItem
            key={`${name}-${price}`}
            name={name}
            price={price}
            orderName={orderName}
            actionButton={<i onClick={() => {

            }} className="fa fa-envelope cursor-pointer hover:text-red-500"></i>}
          />
        ) : <h4>No Items</h4>
      }
    </>

  return (
    <div>
      <div id="list-order">
        <ListCard
          data={data}
          render={(data) => renderListOrderItem(data)}
        />
      </div>

      <div className="summary-box mt-5">
        <div className="flex flex-wrap justify-between">
          <label className="text-sm">Sub Total</label>
          <h4>{`Rp.`} 10000</h4>
        </div>

        <div className="flex flex-wrap justify-between">
          <label className="text-sm">Delivery Fee</label>
          <h4>{`Rp.`} 3000</h4>
        </div>

        <div className="flex flex-wrap justify-between">
          <label className="text-sm">Sub Total</label>
          <h4>{`Rp.`} 13000</h4>
        </div>
      </div>

      <div className="lg:w-full mt-4 flex justify-end">
        <Button title="Finish" onClick={() => { history.push('/') }} />
      </div>
    </div>
  )
}
