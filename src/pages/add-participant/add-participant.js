import React from 'react'
import { useHistory } from "react-router-dom";
import RichInput from '../../components/rich-input'
import Button from '../../components/buttons/button'
import ListCard from '../../components/list-card'
import CardItem from '../../components/card-item'

export default function AddParticipant(props) {
  const history = useHistory()
  const renderListOrderItem = (data) =>
    <>
      {
        data ? data.map(({ name, price, orderName }) =>
          <CardItem
            key={`${name}-${price}`}
            name={name}
            price={price}
            orderName={orderName}
            actionButton={<i onClick={() => {

            }} className="fa fa-trash cursor-pointer hover:text-red-500"></i>}
          />
        ) : <h4>No Items</h4>
      }
    </>

  return (
    <>
      <div className="flex flex-wrap justify-between mb-5">
        <div className="lg:w-full w-full my-2">
          <RichInput label="Participant Name" />
        </div>

        <div className="lg:w-full w-full my-2">
          <RichInput label="Order Name" />
        </div>

        <div className="lg:w-full w-full my-2">
          <RichInput label="Order Price" type="number" />
        </div>

        <div className="lg:w-full my-2 flex justify-end">
          <Button title="Add" onClick={() => { }} />
        </div>
      </div>

      <div id="list-order">
        <ListCard
          data={[
            {
              name: 'Rocky Balboa',
              orderName: 'McNuggets',
              price: '12000'
            },
            {
              name: 'John Rambo',
              orderName: 'KFC Super',
              price: '45000'
            }
          ]}
          render={(data) => renderListOrderItem(data)}
        />
      </div>

      <div className="w-full my-2 flex justify-between">
        <Button title="Back" onClick={() => { history.goBack() }} />
        <Button title="Calculate" onClick={() => { history.push('/finish-calculate') }} />
      </div>
    </>
  )
}
