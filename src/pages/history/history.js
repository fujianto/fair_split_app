import React from 'react'
import { useHistory } from "react-router-dom";

import ListCard from '../../components/list-card'
import Button from '../../components/buttons/button'

export default function History(props) {
  const historyItems = [
    {
      orderTitle: 'Order #1 - 27/02/2021',
    },
    {
      orderTitle: 'Ayam bakar PSP - 28/02/2021',
    }
  ]

  const history = useHistory()

  const renderListItem = (data) =>
    historyItems ? historyItems.map(item =>
      <div className='p-2 bg-gray-300 mb-2'>
        <h3>{item.orderTitle}</h3>
      </div>
    ) : <p>No items</p>

  return (
    <div>
      <h2 className="mb-3">History</h2>

      <ListCard
        data={historyItems}
        render={(data) => renderListItem(data)}
      />

      <div className="lg:w-full mt-4 flex justify-end">
        <Button title="Back" onClick={() => { history.goBack() }} />
      </div>
    </div>
  )
}
