import React from 'react'
import { useHistory } from "react-router-dom";
import RichInput from '../../components/rich-input'
import Button from '../../components/buttons/button'

export default function NewOrder(props) {
  const history = useHistory();

  return (
    <>
      <div className="flex flex-wrap justify-between">
        <div className="lg:w-5/12 w-full my-2">
          <RichInput label="Order Name" />
        </div>

        <div className="lg:w-5/12 w-full my-2">
          <RichInput label="Subtotal" type="number" />
        </div>
      </div>

      <div className="flex flex-wrap justify-between">
        <div className="lg:w-5/12 w-full my-2">
          <RichInput label="Delivery Fee" type="number" value={0} />
        </div>

        <div className="lg:w-5/12 w-full my-2">
          <RichInput label="Order Handler" type="number" value={0} />
        </div>
      </div>

      <div className="flex flex-wrap justify-between items-center mt-5">
        <div className="lg:w-5/12 w-2/3 my-2">
          <h3>Discount: {`${40}%`}</h3>
        </div>

        <div className="lg:w-5/12 w-1/3 my-2 flex justify-end">
          <Button title="Next" onClick={() => { history.push('/add-participant') }} />
        </div>
      </div>
    </>
  )
}
