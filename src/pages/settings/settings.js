import React from 'react'
import { useHistory } from "react-router-dom";

import RichInput from '../../components/rich-input'
import Button from '../../components/buttons/button'

export default function Settings() {
  const history = useHistory();

  return (
    <div>
      <h2>Preferences</h2>

      <div className="flex flex-wrap justify-between">
        <div className="lg:w-full w-full my-2">
          <RichInput label="Currency Symbol" type="text" value={'Rp. '} />
        </div>
      </div>

      <div className="lg:w-full mt-4 flex justify-end">
        <Button title="Back" onClick={() => { history.goBack() }} />
      </div>
    </div>
  )
}
